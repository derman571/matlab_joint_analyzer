%% 4K03 Calculation Driver 
% Inserts estimated inputs and produces transformations, Jacobian, and
% equation of motion components.

clc;
close all;
clear all;
syms 'theta1' 'theta2' 'theta3' 'theta4' 'theta5' real
syms 'the1(t)' 'the2(t)' 'the3(t)' 'the4(t)' 'the5(t)'
syms 'l1' 'l2' 'l3' real
syms 'm1' 'm2' 'm3' 'm4' 'm5' real
syms 'Ixx1' 'Ixy1' 'Ixz1' 'Iyx1' 'Iyy1' 'Iyz1' 'Izx1' 'Izy1' 'Izz1' real
syms 'Ixx2' 'Ixy2' 'Ixz2' 'Iyx2' 'Iyy2' 'Iyz2' 'Izx2' 'Izy2' 'Izz2' real
syms 'Ixx3' 'Ixy3' 'Ixz3' 'Iyx3' 'Iyy3' 'Iyz3' 'Izx3' 'Izy3' 'Izz3' real
syms 'Ixx4' 'Ixy4' 'Ixz4' 'Iyx4' 'Iyy4' 'Iyz4' 'Izx4' 'Izy4' 'Izz4' real
syms 'Ixx5' 'Ixy5' 'Ixz5' 'Iyx5' 'Iyy5' 'Iyz5' 'Izx5' 'Izy5' 'Izz5' real

I = {	[Ixx1,Ixy1,Ixz1;Iyx1,Iyy1,Iyz1;Izx1,Izy1,Izz1],...
	[Ixx2,Ixy2,Ixz2;Iyx2,Iyy2,Iyz2;Izx2,Izy2,Izz2],...
	[Ixx3,Ixy3,Ixz3;Iyx3,Iyy3,Iyz3;Izx3,Izy3,Izz3],...
	[Ixx4,Ixy4,Ixz4;Iyx4,Iyy4,Iyz4;Izx4,Izy4,Izz4],...
	[Ixx5,Ixy5,Ixz5;Iyx5,Iyy5,Iyz5;Izx5,Izy5,Izz5]};
m = [m1;m2;m3;m4;m5];
l = [l1;l2;l3;0;0];
t_i=linspace(1,2*pi,100);
q_f=@(t) pi.*sin(t)./2;%Default function for all joint variables. 
DH=[0,0,0,theta1;0,pi/2,0,theta2+pi/4;l1,0,0,theta3+pi/2;l2,0,0,theta4-pi/4;0,-pi/2,l3,theta5];
q_t(t) = [the1;the2;the3;the4;the5];
q = [theta1;theta2;theta3;theta4;theta5];

[T0_n,T,M,V,G,tau]=DH2JntSpc(DH,q,q_t(t),m,I);%THE APPLIED FUNCTION

L_B = [1.8902, 0, 0; 0, 2.3011, 0; 0,0,3.5365];
L_A = [1.0189,0,0;0,2.754,0;0,0,2.0471];
L_E = [1.2909,0,0;0,2.842,0;0,0,1.9455];
I_a ={L_B,L_A,L_A,L_E,zeros(3)};
m_a = [0.014712;0.0060415;0.0060415;0.0065663;0];
l_a = [0.050;0.050;0.044;0;0];

q_ta=[q_f(t);q_f(t);q_f(t);q_f(t);q_f(t)];
q_fa(t)=[q_f(t);q_f(t);q_f(t);q_f(t);q_f(t)];
T0_na = subs(T0_n,[m l q_t],[m_a l_a q_ta]);
M_a = subs(M,[m l q_t I],[m_a l_a q_ta I_a]);
V_a = subs(V,[m l q_t I],[m_a l_a q_ta I_a]);
G_a = subs(G,[m l q_t I],[m_a l_a q_ta I_a]);
tau_a = subs(tau,[m l q_t I],[m_a l_a q_ta I_a]);

tau_p = double(subs(tau_a,t,t_i));
%for i = [1:length(q)]
%    T{i} = subs(T{i},[m l q_t],[m_a l_a q_ta]);
%end

usrfunct = '~';
q_tnew = sym(zeros(length(q),1)); 
while ~isempty(usrfunct)
    subplot(2,2,1);
    fplot3(T0_na(1,4),T0_na(2,4),T0_na(3,4));
    legend('^0X\rho_n(t)')
    title('Position of End Effector')
    xlabel('X (m)');
    ylabel('Y (m)');
    zlabel('Z (m)');
    subplot(2,2,3);
    fplot(T0_na(1:3,4));
    legend('^0x(t)','^0y(t)','^0z(t)')
    title('Position of End Effector')
    xlabel('t (s)');
    ylabel('displacement (m)');
    axis([0 8 -0.14 0.14]);
    subplot(2,2,2);
    fplot(tau_a);
    legend('Jnt1','Jnt2','Jnt3','Jnt4','Jnt5');
    title('Equation of Motion')
    xlabel('t (s)');
    ylabel('Torque (N*m)');
    axis([0 8 -inf inf]);
    subplot(2,2,4);
    q_ti = double(cell2sym(q_fa(t_i)));
    polarplot(q_ti.',tau_p.');
    legend('Jnt1','Jnt2','Jnt3','Jnt4','Jnt5');
    title('Equation of Motion')
    for i = [1:length(q)]
        fprintf('Enter theta%d(t) function: ',i);
        usrfunct=input('');
        if isempty(usrfunct)
               break;
        end
        q_tnew(i)=usrfunct
    end
    if isempty(usrfunct)
           break;
    end
    T0_na = subs(T0_n,[m l q_t],[m_a l_a q_tnew]);
    M_a = subs(M,[m l q_t I],[m_a l_a q_tnew I_a]);
    V_a = subs(V,[m l q_t I],[m_a l_a q_tnew I_a]);
    G_a = subs(G,[m l q_t I],[m_a l_a q_tnew I_a]);
    tau_a = subs(tau,[m l q_t I],[m_a l_a q_tnew I_a]);
    tau_p = double(subs(tau_a,t,t_i));
    q_fa =symfun(q_tnew,t)
    %close all;
end
close all;
%%%%%%%%%%%%END%%%%%%%%%%%%%%%%%
%% FUNCTION: DH2JntSpc(DH,q,q_t,m,I)
% INPUTS: 
% -symbolic DH parameters with n number of rows (joints) as cell object
% -symbolic variable vector of n length
% -symbolic function vector of n length
% -symbolic mass vector of n length
% -symbolic moment of interia vector of n length 
% OUTPUTS:
% -symbolic transformation matrices as a cell object of n entries
% -symbolic transformation matrix with respect to the base
% -symbolic Lagrange components of the joint torques (M, V, and G)
% -symbolic joint torque vector
function [T0_i,T,M,V,G,tau]=DH2JntSpc(DH,q,q_t,m,I)
%% Initialization of symbolic matrices and other entities.
% Necessary for faster execution time.

    syms t real;
    T0_i=eye(4);
    dq = diff(q_t,t);
    d2q= diff(dq,t);
    V_coriolis=sym(zeros(length(q),1));
    M=sym(zeros(length(q)));
    G=sym(zeros(length(q),1));
    J_i=sym(zeros(6,length(q)));
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
%% DH parameters --> Joint space for n joints.

    for i = [1:length(DH)]
        %% STEP 1: Joint DH parameters --> Transformation matrix $^(i-1)T_i$.
        % Each row of DH parameters is converted into a respective
        % transformation matrix using the general transformation formula.
        
        T{i}=[cos(sym(DH(i,4))),-sin(sym(DH(i,4))),sym(0),sym(DH(i,1));...
        sin(sym(DH(i,4))).*cos(sym(DH(i,2))),cos(sym(DH(i,4))).*cos(sym(DH(i,2))),-sin(sym(DH(i,2))),-sym(DH(i,3)).*sin(sym(DH(i,2)));...
        sin(sym(DH(i,4))).*sin(sym(DH(i,2))),cos(sym(DH(i,4))).*sin(sym(DH(i,2))),cos(sym(DH(i,2))),sym(DH(i,3)).*cos(sym(DH(i,2)));...
        sym(0),sym(0),sym(0),sym(1)];
    
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %% STEP 2: $^(i-1)T_i$ --> $^0T_i$ ($^0T_n$ at end effector) and joint Jacobian J_i
        % The reference to the base uses Euler representation, where the
        % the transformation is based off the previous configuration (post
        % multiply).
        % The rotation vector Z (the 3rd column, first 3 rows of $^0T_i$) and
        % the position vector x_pi (the 4rd column, first 3 rows of $^0T_i$), 
        % are extracted for the use of the Jacobian J_i.
        
        T0_i=T0_i*T{i};
        T{i}=subs(T{i},q,q_t);
        
        z{i}=T0_i(1:3,3);
        xp_i = T0_i(1:3,4);
        
        % NOTE: Since the specific application has no revolute joints, z is
        % valid for all joints. In the future, this would be changed to
        % detect the presence of a translational variable in the 4th column
        % of T(i), which would make the last 3 rows of the corresponding 
        % J_i column = [0;0;0].
        
        for j = [1:i]  
            J_i(1:6,j)=[diff(xp_i,q(j));z{j}];
        end
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %% STEP 3: J_i --> Mass matrix M and gravitational force vector G
        % For each iteration, the mass/inertia of the new joint is added to
        % the mass matrix based on J_i. The same is done with the
        % gravitational term, which only uses the linear portion of J_i.
        %
        % NOTE: The gravity vector is hard set for the specific
        % application. In the future, this will be changed to determine the
        % axis of gravity based on the location of a joint variable in
        % in $^(i-1)T_i$.
        
        M = M + m(i)*J_i(1:3,1:length(q)).'*J_i(1:3,1:length(q))+J_i(4:6,1:length(q)).'*I{i}*J_i(4:6,1:length(q));
        G = G - J_i(1:3,1:length(q)).'*m(i)*[0;0;9.81];
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    end
    %% STEP 4: M --> Centrifugal and Coriolis vector V
    % The derivative of M with respect to time is taken for the centrifugal
    % term, and the derivative of M with respect to each joint variable is
    % taken at the corresponding row for the Coriolis term.
    
    dM_dt=diff(subs(M,q,q_t),t);
    
    for i = [1:length(q)]
        V_coriolis(i)=subs(0.5*dq.'*diff(M,q(i))*dq,q,q_t);
    end
    
    V = dM_dt*dq-V_coriolis;
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %% STEP 5: M, V, G --> Joint torques tau
    % Since the joint variables are real continuous functions with repect
    % to time, symbolic functions replace the symbolic variables. This
    % ensures the proper output when substituting double functions into the
    % returned matrices of T0_n, M, V, G and tau.
    
    T0_i = subs(T0_i,q,q_t);
    M = subs(M,q,q_t);
    G = subs(G,q,q_t);
    
    tau = M*d2q+V+G;
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
end
